package com.springboot.exam.rmc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamRmcApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamRmcApplication.class, args);
	} 
	
}
