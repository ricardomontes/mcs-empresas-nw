package com.springboot.exam.rmc.bean;

import java.util.List;

import com.springboot.exam.rmc.model.Empresa;

public class RespuestaBean {
	String codigo_servicio;
	String descripcion = "";
	List<Empresa> empresas;
	Empresa empresa;
	String msgError = "";
	
	public String getCodigo_servicio() {
		return codigo_servicio;
	}
	
	public void setCodigo_servicio(String codigo_servicio) {
		this.codigo_servicio = codigo_servicio;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public List<Empresa> getEmpresas() {
		return empresas;
	}
	
	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getMsgError() {
		return msgError;
	}

	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
}
