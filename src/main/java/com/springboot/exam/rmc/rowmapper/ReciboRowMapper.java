package com.springboot.exam.rmc.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.exam.rmc.model.Recibo;

public class ReciboRowMapper implements RowMapper<Recibo>{

	@Override
	public Recibo mapRow(ResultSet rs, int rowNum) throws SQLException {
		Recibo recibo = new Recibo();

		recibo.setId_empresa(rs.getInt("id_empresa"));
		recibo.setNro_recibo(rs.getString("nro_recibo"));
		recibo.setMonto_emitido(rs.getFloat("monto_emitido"));
		recibo.setFg_retencion(rs.getInt("fg_retencion"));
		
		return recibo;
	}

}
