package com.springboot.exam.rmc.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.exam.rmc.model.Empresa;

public class EmpresaRowMapper implements RowMapper<Empresa>{

	@Override
	public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
		Empresa empresa = new Empresa();
		
		empresa.setId_empresa(rs.getInt("id_empresa"));
		empresa.setRuc(rs.getString("ruc"));
		empresa.setRazon_social(rs.getString("razon_social"));
		empresa.setEstado_actual(rs.getString("estado_actual"));
		
		return empresa;
	}

}
