package com.springboot.exam.rmc.model;

public class Recibo {
	
	private Integer id_empresa;
	private String nro_recibo;
	private float monto_emitido;
	private int fg_retencion;
		
	public Recibo() {
		
	}
	
	public Recibo(Integer id_empresa, String nro_recibo, float monto_emitido, int fg_retencion) {
	
		this.id_empresa = id_empresa;
		this.nro_recibo = nro_recibo;
		this.monto_emitido = monto_emitido;
		this.fg_retencion = fg_retencion;
	}

	public Integer getId_empresa() {
		return id_empresa;
	}

	public void setId_empresa(Integer id_empresa) {
		this.id_empresa = id_empresa;
	}

	public String getNro_recibo() {
		return nro_recibo;
	}

	public void setNro_recibo(String nro_recibo) {
		this.nro_recibo = nro_recibo;
	}

	public float getMonto_emitido() {
		return monto_emitido;
	}

	public void setMonto_emitido(float monto_emitido) {
		this.monto_emitido = monto_emitido;
	}

	public int getFg_retencion() {
		return fg_retencion;
	}

	public void setFg_retencion(int fg_retencion) {
		this.fg_retencion = fg_retencion;
	}

	@Override
	public String toString() {
		return "Recibo [id_empresa=" + id_empresa + ", nro_recibo=" + nro_recibo + ", monto_emitido=" + monto_emitido
				+ ", fg_retencion=" + fg_retencion + "]";
	}
	
}
