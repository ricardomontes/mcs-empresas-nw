package com.springboot.exam.rmc.service;

import java.util.List;

import com.springboot.exam.rmc.model.Empresa;
import com.springboot.exam.rmc.model.Recibo;

public interface EmpresaService {
	
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id);
	void saveEmpresa(Empresa empresa);
	void saveRecibo(Recibo recibo);
	
}
