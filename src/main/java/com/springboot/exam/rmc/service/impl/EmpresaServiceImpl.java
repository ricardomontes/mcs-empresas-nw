package com.springboot.exam.rmc.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.exam.rmc.dao.impl.EmpresaDaoImpl;
import com.springboot.exam.rmc.dao.impl.ReciboDaoImpl;
import com.springboot.exam.rmc.model.Empresa;
import com.springboot.exam.rmc.model.Recibo;
import com.springboot.exam.rmc.service.EmpresaService;

@Service
public class EmpresaServiceImpl implements EmpresaService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmpresaDaoImpl _empresalDao;

	@Autowired
	private ReciboDaoImpl _reciboDao;
	
	@Override
	public List<Empresa> getAllEmpresas() {
		List<Empresa> listaEmpresas = _empresalDao.getAllEmpresas();
				
		if(listaEmpresas != null && listaEmpresas.size() > 0) {
			for (Empresa empresa : listaEmpresas) {
				empresa.setRh_emitidos(_reciboDao.getAllRecibos(empresa.getId_empresa()));
			}
		}		
		
		return listaEmpresas;
	}

	@Override
	public Empresa getEmpresa(Integer id) {
		
		return _empresalDao.getEmpresa(id);
	}

	@Override
	public void saveEmpresa(Empresa empresa) {
		try {
			_empresalDao.saveEmpresa(empresa);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
	}

	@Override
	public void saveRecibo(Recibo recibo) {
		try {
			_reciboDao.saveRecibo(recibo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
	}

}
