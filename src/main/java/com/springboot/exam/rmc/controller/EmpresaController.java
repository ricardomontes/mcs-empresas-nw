package com.springboot.exam.rmc.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.exam.rmc.bean.RespuestaBean;
import com.springboot.exam.rmc.model.Empresa;
import com.springboot.exam.rmc.service.impl.EmpresaServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/examen/empresa")
@Api(description = "Api para el mantenimiento de los datos de una empresa")
public class EmpresaController {
	
	@Autowired
	private EmpresaServiceImpl _empresaService;
	
	@GetMapping(value = "/", produces = "application/json")
	@ApiOperation("Listado general de todas las empresas")
	public RespuestaBean getAllEmpresas(){
		RespuestaBean res = new RespuestaBean();
		
		try {
			res.setCodigo_servicio("0000");
			res.setEmpresas(_empresaService.getAllEmpresas()); 
		} catch (Exception e) {
			res.setCodigo_servicio("9999");
			res.setMsgError(e.getMessage());
		}
		
		return res;
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	@ApiOperation("Metodo para buscar a una persona por su ID")
	public RespuestaBean getEmpresa(@ApiParam("ID de la empresa, Integer") @PathVariable ("id") Integer id){
		RespuestaBean res = new RespuestaBean();
		
		try {
			Empresa emp = _empresaService.getEmpresa(id);
			
			if (emp == null || emp.getId_empresa() == null) {
				res.setCodigo_servicio("9998");
				res.setMsgError("No se pudo encontrar la empresa con el id: " + id);
			} else {
				res.setCodigo_servicio("0000");
				res.setEmpresa(_empresaService.getEmpresa(id)); 
			}
			
			
		} catch (Exception e) {
			res.setCodigo_servicio("9999");
			res.setMsgError(e.getMessage());
		}
		
		return res;
	}
	
	@PostMapping(value = "/", produces = "application/json")
	@ApiOperation("Metodo para insertar una nueva empresa")
	public RespuestaBean saveEmpresa(@RequestBody Empresa empresa){
		RespuestaBean res = new RespuestaBean();
		
		try {
			_empresaService.saveEmpresa(empresa);
			res.setCodigo_servicio("0000");
			res.setDescripcion("Empresa registrada con exito");
		} catch (Exception e) {
			res.setCodigo_servicio("9997");
			res.setMsgError("No se pudo registrar la empresa");
		}
		
		
		return res;
	}

}
