package com.springboot.exam.rmc.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.exam.rmc.bean.RespuestaBean;
import com.springboot.exam.rmc.model.Recibo;
import com.springboot.exam.rmc.service.impl.EmpresaServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/examen/recibo")
@Api(description = "Api para el mantenimiento de los datos de un recibo")
public class ReciboController {
	
	@Autowired
	private EmpresaServiceImpl _empresaService;
	
	@PostMapping(value = "/", produces = "application/json")
	@ApiOperation("Metodo para insertar una nueva empresa")
	public RespuestaBean saveRecibo(@RequestBody Recibo recibo){
		RespuestaBean res = new RespuestaBean();
		
		try {
			_empresaService.saveRecibo(recibo);
			res.setCodigo_servicio("0000");
			res.setDescripcion("Recibo registrado con exito");
		} catch (Exception e) {
			res.setCodigo_servicio("9997");
			res.setMsgError("No se pudo registrar el recibo");
		}
		
		return res;
	}

}
