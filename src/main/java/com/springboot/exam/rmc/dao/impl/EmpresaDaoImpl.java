package com.springboot.exam.rmc.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.exam.rmc.dao.EmpresaDao;
import com.springboot.exam.rmc.model.Empresa;
import com.springboot.exam.rmc.rowmapper.EmpresaRowMapper;

@Repository
public class EmpresaDaoImpl extends JdbcDaoSupport implements EmpresaDao {

	public EmpresaDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public List<Empresa> getAllEmpresas() {
		logger.debug("::::: Mensaje de prueba :::::::");
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		
		String sql = " SELECT id_empresa, ruc, razon_social, estado_actual " + 
					 " FROM microservicios.empresa";
		
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			logger.debug("Se han listado "+listaEmpresas.size()+" empresas");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return listaEmpresas;
	}

	@Override
	public Empresa getEmpresa(Integer id) {
		logger.debug("::::: Mensaje de prueba :::::::");
		Empresa empresa = new Empresa();	
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		
		String sql = " SELECT id_empresa, ruc, razon_social, estado_actual " + 
				" FROM microservicios.empresa where id_empresa='"+id+"'";
				
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			
			empresa = listaEmpresas.get(0);
			
			logger.debug("Se ha traido a la empresa "+listaEmpresas.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return empresa;
	}

	@Override
	public void saveEmpresa(Empresa empresa) {
		
		String sql = "insert into microservicios.empresa (ruc, razon_social, estado_actual) "  
				+ "values (?, ?, ?);";
		
		Object[] params = { empresa.getRuc(), empresa.getRazon_social(), empresa.getEstado_actual()};
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado a la empresa "+empresa.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

}
