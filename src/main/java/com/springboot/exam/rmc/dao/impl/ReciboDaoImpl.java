package com.springboot.exam.rmc.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.exam.rmc.dao.ReciboDao;
import com.springboot.exam.rmc.model.Recibo;
import com.springboot.exam.rmc.rowmapper.ReciboRowMapper;

@Repository
public class ReciboDaoImpl extends JdbcDaoSupport implements ReciboDao {

	public ReciboDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public List<Recibo> getAllRecibos(Integer id_empresa) {
		logger.debug("::::: Mensaje de prueba :::::::");
		List<Recibo> listaRecibos = new ArrayList<Recibo>();
		
		String sql = " SELECT id_empresa,nro_recibo,monto_emitido, fg_retencion " + 
					 " FROM microservicios.recibo where id_empresa = " + id_empresa;
		
		try {
			
			RowMapper<Recibo> empresaRow = new ReciboRowMapper();
			listaRecibos = getJdbcTemplate().query(sql, empresaRow);
			logger.debug("Se han listado "+listaRecibos.size()+" recibos");
			
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return listaRecibos;
	}

	@Override
	public void saveRecibo(Recibo recibo) {
		
		String sql = "insert into microservicios.recibo (id_empresa,nro_recibo,monto_emitido, fg_retencion) "  
				+ "values (?, ?, ?, ?);";
		
		Object[] params = { recibo.getId_empresa(), recibo.getNro_recibo(), recibo.getMonto_emitido(), recibo.getFg_retencion()};
		int[] tipos = { Types.INTEGER, Types.VARCHAR, Types.FLOAT, Types.TINYINT};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado el recibo "+recibo.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

}
