package com.springboot.exam.rmc.dao;

import java.util.List;

import com.springboot.exam.rmc.model.Empresa;

public interface EmpresaDao {
	
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id);
	void saveEmpresa(Empresa empresa);

}
