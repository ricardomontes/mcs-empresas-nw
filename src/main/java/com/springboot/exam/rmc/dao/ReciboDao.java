package com.springboot.exam.rmc.dao;

import java.util.List;

import com.springboot.exam.rmc.model.Recibo;

public interface ReciboDao {
	
	List<Recibo> getAllRecibos(Integer id_empresa);
	void saveRecibo(Recibo recibo);

}
